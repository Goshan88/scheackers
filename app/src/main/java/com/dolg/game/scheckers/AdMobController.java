package com.dolg.game.scheckers;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

/**
 * Created by Гоша on 20.03.2018.
 */

public class AdMobController implements AdsControllerBase{

    private static final String ADMOB_ID=Const.ADMOB_BANNER;
    private static final int REQUEST_TIMEOUT = 30000;
    private AdView adView;
    private Context c;
    private long last;
    private InterstitialAd mInterstitialAd;
    //private  boolean view_admob = true;

    public AdMobController(Context activity, RelativeLayout layout){
        this.c = activity;
        createView(layout);
        last = System.currentTimeMillis() - REQUEST_TIMEOUT;
    }

    public void createView(RelativeLayout layout){
        //if (PreferencesHelper.isAdsDisabled()) return;
        adView = new AdView(c);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(ADMOB_ID);
        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layout.addView(adView, adParams);
        adView.loadAd(new AdRequest.Builder().build());
    }

    public void show(boolean show){
        if(adView == null) return;
        adView.setVisibility((show) ? View.VISIBLE : View.GONE);
        if (show && (System.currentTimeMillis() - last > REQUEST_TIMEOUT)){
            last = System.currentTimeMillis();
            adView.loadAd(new AdRequest.Builder().build());
        }
    }

    public void Interstitial(boolean view_admob){
        if(view_admob == true) {
            MobileAds.initialize(this.c,Const.ADMOB_USER_ID);
            mInterstitialAd = new InterstitialAd(this.c);
            mInterstitialAd.setAdUnitId(Const.ADMOB_INTERSTITIAL);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.show();
        }else{
        }
    }

    @Override
    public void onStart(){}

    @Override
    public void onDestroy(){}

    @Override
    public void onResume(){}

    @Override
    public void onStop(){}
}
