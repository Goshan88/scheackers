package com.dolg.game.scheckers;

import android.widget.RelativeLayout;

/**
 * Created by Гоша on 20.03.2018.
 */

public interface AdsControllerBase {
    public void createView(RelativeLayout layout);
    public void show(boolean show);
    public void onStart();
    public void onDestroy();
    public void onResume();
    public void onStop();
}
