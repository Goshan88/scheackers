package com.dolg.game.scheckers;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.pixplicity.easyprefs.library.Prefs;

import core.algorithm.AlgorithmType;

public class ChessBoardActivity extends AppCompatActivity implements CustomAlertDialog.OnButtonClick{
    private ChessBoardView chessBoardView;
    private LinearLayout rootLayout;
    TextView statusTextView;
    private int difficult_level = 1;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;
    private MenuItem Replay, changeGirl,imageGirl;
    private int GirlOnIcon= com.dolg.game.scheckers.R.drawable.girl_1_1;

    private CustomAlertDialog customAlertDialog;
    private ImageView iconGirl;
    private ImageHelper myImageHelper;
    private Handler mainHandler;
    private Context myContext;
    private Menu menu;

    //admob
    AdsControllerBase ads;
    RelativeLayout layout;
    private InterstitialAd mInterstitialAd;
    private FirebaseAnalytics mFirebaseAnalitics;
    private int cntLoad=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.dolg.game.scheckers.R.layout.activity_chess_board);
        rootLayout = (LinearLayout) findViewById(com.dolg.game.scheckers.R.id.root_layout);
        statusTextView = (TextView) findViewById(com.dolg.game.scheckers.R.id.statusTextView);
        setLayoutOrientation();

        //Initialize the Prefs class
        new Prefs.Builder().setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true).build();

        loadCntStartGame();

        initHandler();
        myContext = this;

        Log.i("Create ChessBoardActivity");

        Toolbar toolbar = (Toolbar) findViewById(com.dolg.game.scheckers.R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(com.dolg.game.scheckers.R.string.score)
                .withIcon(GoogleMaterial.Icon.gmd_dashboard).withSelectable(false);

        Drawer Navigation = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem()
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener(){
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem){
                        Log.i("onItemClick on navigation bar");
                        if (drawerItem instanceof Nameable) {
                            switch (position) {
                                case Const.ITEM_SCORES:
                                    Intent scores_int = new Intent(myContext, ScoresActivity.class);
                                    startActivity(scores_int);
                                    break;
                                default:
                                    break;
                            }
                        }
                        return true;
                    }
                })
                .build();


        myImageHelper = new ImageHelper(this,mainHandler);
        newGame();
        choiceGirlDialog();//если не вызывать то метод установки иконки не находит фотодевушки

        //грузим рекламу
        layout = (RelativeLayout)findViewById(com.dolg.game.scheckers.R.id.relative_chessboard);
        ads = new AdMobController(this,layout);
        ads.show(Const.advertisement_show);
        if((Const.advertisement_show)&&(cntLoad>Const.CNT_AFTER_LOAD_AD)){
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(Const.ADMOB_INTERSTITIAL);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());

            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }else{
                        Log.i("Interstitial not load");
                    }
                }

                @Override
                public void onAdOpened() {
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    Log.i("Interstitial onAdFailedToLoad");
                }
            });

        }


        mFirebaseAnalitics = FirebaseAnalytics.getInstance(this);


    }

    void choiceGirlDialog(){
        //alert dialog
        customAlertDialog = new CustomAlertDialog(ChessBoardActivity.this);
        customAlertDialog.setListener(this);
        customAlertDialog.show();
    }

    private void loadCntStartGame(){
        int tmp = Prefs.getInt(Const.KEY_CNT_LOAD,0);
        cntLoad = ++tmp;
        Prefs.putInt(Const.KEY_CNT_LOAD,tmp);
        Log.i("cntLoad");
        Log.i(String.valueOf(cntLoad));
    }

    void newGame() {
        if (chessBoardView == null) {
            chessBoardView = new ChessBoardView(this, statusTextView, AlgorithmType.COMPUTER, difficult_level, myImageHelper);
            chessBoardView.invalidate();
            rootLayout.addView(chessBoardView, 0);
        }else{
            chessBoardView.newGame();
        }
        myImageHelper.beginGame();
    }

    private void initHandler() {
        mainHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case Const.CHANGE_ICON:
                        Log.i("Handler Change Icon");
                        Log.i(String.valueOf(msg.arg1));
                        changeImageIcon(msg.arg1);
                        break;
                    case Const.SHOW_GIRL:
                        Log.i("Handler SHOW_GIRL");
                        showAlertImage(msg.arg1);
                        break;
                    case Const.ERROR_NORES:
                        Log.i("Handler ERROR_NO_RES");
                        dissmisAlertImage();
                        Toast.makeText(myContext, com.dolg.game.scheckers.R.string.error_no_image_res,Toast.LENGTH_SHORT).show();
                        break;
                    case Const.REGAME:
                        Log.i("Handler REGAME");
                        newGame();
                        break;
                    case Const.CHANGE_DIFFICULTY:
                        Log.i("Handler CHANGE_DIFFICULTY");
                        chessBoardView.setmDifficulty(msg.arg1);
                        newGame();
                        break;
                    case Const.CHANGE_GIRL:
                        Log.i("Handler CHANGE_GIRL");
                        choiceGirlDialog();
                        newGame();
                        break;
                    case Const.COMPLETELY_WON:
                        Log.i("Handler COMPLETELY_WON");
                        dissmisAlertImage();
                        Toast.makeText(myContext, com.dolg.game.scheckers.R.string.completely_won,Toast.LENGTH_LONG).show();
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }


    @Override
    public void onImageSwitchClick(int numb){
        Log.i(String.valueOf(numb));

        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, numb);
        mFirebaseAnalitics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        myImageHelper.choiceGirl(numb);
        dissmisAlertImage();
    }

    @Override
    public void onImageButtonClick(){
        Log.i("onImageButtonClick");
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.CONTENT_TYPE, myImageHelper.getIdGirl());
        bundle.putInt(FirebaseAnalytics.Param.CONTENT, myImageHelper.getIdImage());
        mFirebaseAnalitics.logEvent(FirebaseAnalytics.Event.CHECKOUT_PROGRESS, bundle);
        myImageHelper.clickOnImageGirl();
    }


    public void showAlertImage(int id){
        if(customAlertDialog.isShowing()) {

            customAlertDialog.changeGirlImage(id);
        }else{
            customAlertDialog = new CustomAlertDialog(ChessBoardActivity.this, id);
            customAlertDialog.setListener(this);
            customAlertDialog.show();
        }
    }

    public void dissmisAlertImage(){
        if(customAlertDialog != null) {
            customAlertDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.dolg.game.scheckers.R.menu.menu_actionbar, menu);
        Replay = menu.findItem(com.dolg.game.scheckers.R.id.replay);
        changeGirl = menu.findItem(com.dolg.game.scheckers.R.id.change_girl);
        imageGirl =menu.findItem(com.dolg.game.scheckers.R.id.image_girl);
        this.menu = menu;
        imageGirl.setIcon(GirlOnIcon);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onResume(){
           //myImageHelper.loadIconLastGirl();
        GirlOnIcon = myImageHelper.getIdCurrentGirl();
        super.onResume();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void changeImageIcon(int id){
        GirlOnIcon = id;
        imageGirl.setIcon(id);
    }

    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence("statusText", statusTextView.getText());
        Log.i("onSaveInstanceState  ChessBoardActivity");
    }

    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        statusTextView.setText(savedInstanceState.getCharSequence("statusText"));
        Log.i("onRestoreInstanceState  ChessBoardActivity");
    }

    private void setLayoutOrientation(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        if (height < width)
            rootLayout.setOrientation(LinearLayout.HORIZONTAL);
        else
            rootLayout.setOrientation(LinearLayout.VERTICAL);
        Log.i("setLayoutOrientation  ChessBoardActivity");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case com.dolg.game.scheckers.R.id.replay:
                Log.i(("Click replay"));
                myImageHelper.reGame();
                break;
            case com.dolg.game.scheckers.R.id.change_girl:
                Log.i(("Click change girl"));
                myImageHelper.changeGirl();
                break;
            case com.dolg.game.scheckers.R.id.image_girl:
                Log.i(("Click image girl"));
                myImageHelper.showImageCurrentGirl();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
