package com.dolg.game.scheckers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

import core.algorithm.AlgorithmType;
import core.algorithm.AlphaBetaPruning;
import core.algorithm.IAlgorithm;
import core.board.CellRect;
import core.board.BoardCell;
import core.rules.GameBoard;
import core.rules.Move;
import core.rules.Player;

import java.util.List;

public class ChessBoardView extends View implements OnTouchListener {
    private final int HIGHLIGHT_COLOR = Color.GREEN;
    private float screenW;
    private float screenH;
    private float cellSize;
    private float boardMargin;
    private Thread clickThread;
    private Canvas canvas = null;
    private Paint paint = null;
    private TextView statusTextView;
    public GameBoard gameBoard;
    private BoardCell previousCell;
    private BoardCell requiredMoveCell;
    private IAlgorithm algorithm;
    private Player player;
    private ImageHelper mImageHelper;
    private int mDifficulty=1;

    public ChessBoardView(Context context, TextView statusTextView, AlgorithmType type, int difficulty, ImageHelper _imageHelper) {
        super(context);
        this.statusTextView = statusTextView;
        clickThread = null;
        gameBoard = new GameBoard();
        previousCell = null;
        requiredMoveCell = null;
        player = Player.WHITE;
        //statusTextView.setText("Status: Turn of " + player.getPlayerName() + " player...");
        mDifficulty = difficulty;
        algorithm = new AlphaBetaPruning(gameBoard, difficulty);
        mImageHelper = _imageHelper;
        //Log.i("constructor ChessBoardView");
    }

    @Override
    protected void onDraw(Canvas c){
        canvas = c;
        super.onDraw(canvas);
        paint = new Paint();
        drawBoard();
        drawCells();
        drawPieces();
        this.setOnTouchListener(this);
        //Log.i("onDraw ChessBoardView");
    }


    public void newGame(){
        //Log.i("ChessBoardView newGame");
        clickThread = null;
        gameBoard = new GameBoard();
        previousCell = null;
        requiredMoveCell = null;
        player = Player.WHITE;
        algorithm = new AlphaBetaPruning(gameBoard, mDifficulty);
        invalidate();
    }

    public void setmDifficulty(int level){
        //Log.i("Change mDifficulty");
        //Log.i(String.valueOf(level));
        mDifficulty = level;
        algorithm.setMaxDepth(mDifficulty);
    }

    public int getmDifficulty(){
        return mDifficulty;
    }

    private void drawBoard() {
        paint.setStyle(Paint.Style.FILL);
        float boardSize = screenW - 2*boardMargin;
        cellSize = boardSize / GameBoard.CELL_COUNT;
        //Log.i("drawBoard ChessBoardView");
    }

    //bitmap для клетки
    private Bitmap getBitmap(int id){
        Bitmap btmmapSource;
        Bitmap bitmap_cell;
        btmmapSource = BitmapFactory.decodeResource(getResources(),id);
        bitmap_cell = Bitmap.createScaledBitmap(btmmapSource,(int)Math.round(cellSize),(int)Math.round(cellSize),true);
        //Log.i("getBitmap ChessBoardView");
        return bitmap_cell;
    }

    private void drawCells() {
        for (int row = 0; row < GameBoard.CELL_COUNT; ++row) {
            for (int col = 0; col < GameBoard.CELL_COUNT; ++col) {
                CellRect rect = new CellRect(boardMargin + cellSize * col, boardMargin + cellSize * row, boardMargin + cellSize * (col + 1), boardMargin + cellSize * (row + 1));
                gameBoard.getCell(row, col).setRect(rect);
                if ((row + col) % 2 == 0) {
                    paint.setColor(Color.GRAY);//белые клетки
                    canvas.drawBitmap(getBitmap(com.dolg.game.scheckers.R.mipmap.white_wood), rect.getLeft(), rect.getTop(), paint);
                } else {
                    if (gameBoard.getCell(row, col).isHighlight()){
                        paint.setColor(HIGHLIGHT_COLOR);//когда шашка выбрана
                        canvas.drawRect(rect.getLeft(), rect.getTop(), rect.getRight(), rect.getBottom(), paint);
                    }else{
                        paint.setColor(Color.BLACK);
                        canvas.drawBitmap(getBitmap(com.dolg.game.scheckers.R.mipmap.black_wood), rect.getLeft(), rect.getTop(), paint);
                    }
                }
            }
        }
       // Log.i("drawCells ChessBoardView");
    }


    private void drawPieces() {
        int image_id;

        List<Move> moves = gameBoard.getAllAvailiableMoves(Player.WHITE);

        paint.setAntiAlias(true);
        float radius = cellSize/3;
        for (int row = 0; row < GameBoard.CELL_COUNT; ++row) {
            for (int col = 0; col < GameBoard.CELL_COUNT; ++col) {
                if (gameBoard.getCell(row, col).getCondition() == BoardCell.WHITE_PIECE)
                   // paint.setColor(WHITE_PIECE_COLOR);
                    image_id = com.dolg.game.scheckers.R.mipmap.white_checker_v3;
                    //image_id = R.mipmap.white_checker_highlight;
                else if (gameBoard.getCell(row, col).getCondition() == BoardCell.BLACK_PIECE)
                    //paint.setColor(BLACK_PIECE_COLOR);
                    image_id = com.dolg.game.scheckers.R.mipmap.black_checker_v3;
                else
                    continue;
                float circleCenterX = gameBoard.getCell(row, col).getRect().getLeft()/* + cellSize/2*/;
                float circleCenterY = gameBoard.getCell(row, col).getRect().getTop()/* + cellSize/2*/;
                canvas.drawBitmap(getBitmap(image_id),circleCenterX, circleCenterY, paint);
                if (gameBoard.getCell(row, col).isKingPiece())
                    if(gameBoard.getCell(row, col).getCondition() == BoardCell.WHITE_PIECE) {
                        canvas.drawBitmap(getBitmap(com.dolg.game.scheckers.R.mipmap.white_crowd_v5), circleCenterX, circleCenterY, paint);
                    }else{
                        canvas.drawBitmap(getBitmap(com.dolg.game.scheckers.R.mipmap.black_crowd_v3), circleCenterX, circleCenterY, paint);
                    }
                //рисуем все возможные ходы
                for (Move m : moves) {
                    //m.getToCell().setHighlight(false);
                    if ((m.getFromCell().getCol() == col) && (m.getFromCell().getRow() == row)) {
                        if (gameBoard.getCell(row, col).isKingPiece()) {
                            canvas.drawBitmap(getBitmap(com.dolg.game.scheckers.R.mipmap.white_crowd_highlight), circleCenterX, circleCenterY, paint);
                        } else {
                            canvas.drawBitmap(getBitmap(com.dolg.game.scheckers.R.mipmap.white_checker_highlight), circleCenterX, circleCenterY, paint);
                        }
                    }
                }
            }
        }
       // Log.i("drawPieces ChessBoardView");
    }


    @Override
    public void onSizeChanged (int w, int h, int oldw, int oldh){
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
        //Log.i("onSizeChanged ChessBoardView");
    }

    @Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        if (height > width) {
            setMeasuredDimension(width, width);
        }
        else {
            setMeasuredDimension(height, height);
        }
        //Log.i("onMeasure ChessBoardView");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //Log.i("onTouch ChessBoardView");
        if (clickThread != null && clickThread.isAlive())
            return false;
        if (gameBoard.hasWon(player) || gameBoard.hasWon(player.getOpposite()))
            return false;
        float x = event.getX();
        float y = event.getY();

        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                //Log.i("case MotionEvent.ACTION_DOWN");
                for (int row = 0; row < GameBoard.CELL_COUNT; ++ row) {
                    for (int col = 0; col < GameBoard.CELL_COUNT; ++col) {
                        if (gameBoard.getCell(row, col).getCondition() == player.getOpposite().getPieceColor()){
                            continue;}
                        if (gameBoard.getCell(row, col).getCondition() == BoardCell.EMPTY_CELL
                                && !gameBoard.getCell(row, col).isHighlight())
                            continue;
                        if (gameBoard.getCell(row, col).getRect().contains(x, y)) {
                            if (requiredMoveCell != null) {
                                //Log.i("requiredMoveCell != null");
                                boolean requiredMove = false;
                                for (Move eatMove : gameBoard.getAvailiableMoves(requiredMoveCell)) {
                                    if (gameBoard.getCell(row, col) == eatMove.getToCell()) {
                                        requiredMove = true;
                                    }
                                }
                                if (!requiredMove)
                                    continue;
                            }
                            final int finalRow = row;
                            final int finalCol = col;
                            clickThread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    cellTouch(gameBoard.getCell(finalRow, finalCol));
                                }
                            });
                            clickThread.start();
                            mImageHelper.startPlay();
                        }
                    }
                }
                return true;
        }
        return false;
    }

    private void cellTouch(BoardCell cell) {
        //Log.i("cellTouch ChessBoardView");
        if (cell.getCondition() == player.getPieceColor()) {
            highlightMoves(cell, player);
        }else{
            doMove(cell);
        }
        this.post(new Runnable() {
            public void run() {
                invalidate();
            }
        });
    }

    private void highlightMoves(BoardCell cell, final Player player) {
        //Log.i("highlightMoves ChessBoardView");
        if (previousCell != null) {
            //Log.i("previousCell != null");
            List<Move> moves = gameBoard.getAvailiableMoves(previousCell);
            for (Move m : moves) {
                m.getToCell().setHighlight(false);
            }
            previousCell.setHighlight(false);
        }

        boolean moveIsAvailiable = false;
        for (Move m : gameBoard.getAllAvailiableMoves(player)) {
            if (m.getFromCell() == cell)
                moveIsAvailiable = true;
        }
        if (!moveIsAvailiable) {
            this.post(new Runnable() {
                public void run() {
                    //statusTextView.setText("Status: " + player.getPlayerName() + " player have to eat...");
                }
            });
            return;
        }

        previousCell = cell;
        cell.setHighlight(true);
        List<Move> moves = gameBoard.getAvailiableMoves(cell);
        for (Move m : moves) {
            m.getToCell().setHighlight(true);
            //Log.i(m.toString());
        }
    }

    private void doMove(BoardCell cell) {
        //Log.i("doMove ChessBoardView");
        if (previousCell == null)
            return;

        requiredMoveCell = (requiredMoveCell == previousCell) ? null : requiredMoveCell;

        previousCell.setHighlight(false);
        List<Move> moves = gameBoard.getAvailiableMoves(previousCell);
        for (Move m : moves) {
            m.getToCell().setHighlight(false);
        }
        previousCell = null;
        for (Move m : moves) {
            if (m.getToCell().getRow() == cell.getRow() && m.getToCell().getCol() == cell.getCol()) {
                if (m.getEatCell() != null) {
                    //Log.i("m.getEatCell() != null");
                    gameBoard.doMove(m);
                    if (gameBoard.isExistsNextEatMove(m.getToCell(), m.getToCell(), null)) {
                       // Log.i("gameBoard.isExistsNextEatMove(m.getToCell(), m.getToCell(), null)");
                        requiredMoveCell = m.getToCell();
                        highlightMoves(requiredMoveCell, player);
                        this.post(new Runnable() {
                            public void run() {
                                //statusTextView.setText("Status: Turn of " + player.getPlayerName() + " player...");
                            }
                        });
                        return;
                    }
                }
                else {
                    //Log.i("m.getEatCell() == null");
                    gameBoard.doMove(m);
                }
                if (gameBoard.hasWon(player)) {
                    //Log.i("gameBoard.hasWon(player)__1");
                    if(player.getPlayerName().equals(Player.WHITE)){
                        mImageHelper.winComputer();//почему то так
                    }else{
                        mImageHelper.winUser();
                    }
                    this.post(new Runnable() {
                        public void run() {
                            //statusTextView.setText("Status: " + player.getPlayerName() + " player has won!");
                            invalidate();
                        }
                    });
                    return;
                }
                if (algorithm.getAlgorithmType() == AlgorithmType.HUMAN) {
                    //Log.i("algorithm.getAlgorithmType() == AlgorithmType.HUMAN");
                    player = player.getOpposite();
                    break;
                }
                int i = 0;
                do {
                    if (gameBoard.getAllAvailiableMoves(player.getOpposite()).isEmpty())
                        break;
                    if (i > 0) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                            e.getLocalizedMessage();
                        }
                    }
                    i++;
                    //Log.i("algorithm.getAlgorithm(player.getOpposite())");
                    algorithm.getAlgorithm(player.getOpposite());
                    boolean eatMove = false;
                    if (algorithm.getOpponentMove().getEatCell() != null
                            && algorithm.getOpponentMove().getEatCell().getRect() != null)
                        eatMove = true;
                    this.post(new Runnable() {
                        public void run() {
                            //statusTextView.setText("Status: Turn of " + player.getOpposite().getPlayerName() + " player...");
                            highlightMoves(algorithm.getOpponentMove().getFromCell(), player.getOpposite());
                            invalidate();
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.getLocalizedMessage();
                    }
                    algorithm.getOpponentMove().getFromCell().setHighlight(false);
                    List<Move> compMoves = gameBoard.getAvailiableMoves(algorithm.getOpponentMove().getFromCell());
                    for (Move cm : compMoves) {
                        cm.getToCell().setHighlight(false);
                    }
                    gameBoard.doMove(algorithm.getOpponentMove());
                    if (!eatMove)
                        break;
                    if (gameBoard.hasWon(player.getOpposite())) {
                        if(player.getPlayerName().equals(Player.WHITE)){
                            mImageHelper.winUser();
                        }else{
                            mImageHelper.winComputer();
                        }
                        this.post(new Runnable() {
                            public void run() {
                                invalidate();
                                //statusTextView.setText("Status: " + player.getOpposite().getPlayerName() + " player has won!");
                            }
                        });
                        return;
                    }
                    this.post(new Runnable() {
                        public void run() {
                            invalidate();
                        }
                    });
                } while (gameBoard.isExistsNextEatMove(algorithm.getOpponentMove().getToCell(), algorithm.getOpponentMove().getToCell(), null));
                break;
            }
        }

        this.post(new Runnable() {
            public void run() {
               // statusTextView.setText("Status: Turn of " + player.getPlayerName() + " player...");
            }
        });
    }
}
