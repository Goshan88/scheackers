package com.dolg.game.scheckers;

/**
 * Created by Гоша on 19.04.2018.
 */

public interface Const {

    boolean advertisement_show = true;
    String KEY_CNT_LOAD = "key_cnt_load";
    int CNT_AFTER_LOAD_AD = 2;

    String TAG = "Scheckers";
    //navigation panel
    int LEFT_ITEM_CITY = 1;
    int LEFT_ITEM_SETTINGS = 2;
    int LEFT_ITEM_HELP = 3;
    int LEFT_ITEM_MAIL = 4;

    //numb girl
    int CNT_GIRLS = 9;
    int i_GIRL1 = 1;
    int MAX_CNT_GIRL1 = 6;
    int i_GIRL2 = 2;
    int MAX_CNT_GIRL2 = 6;
    int i_GIRL3 = 3;
    int MAX_CNT_GIRL3 = 3;
    int i_GIRL4 = 4;
    int MAX_CNT_GIRL4 = 7;
    int i_GIRL5 = 5;
    int MAX_CNT_GIRL5 = 2;
    int i_GIRL6 = 6;
    int MAX_CNT_GIRL6 = 5;
    int i_GIRL7 = 7;
    int MAX_CNT_GIRL7 = 4;
    int i_GIRL8 = 8;
    int MAX_CNT_GIRL8 = 8;
    int i_GIRL9 = 9;
    int MAX_CNT_GIRL9 = 3;
    int cntBegin = 1;
    String strGirlName ="girl_";

    //size image in action bar
    int image_height = 90;
    int image_widht = 60;
    int image_margin = 30;

    //Handler
    int CHANGE_ICON = 0;
    int SHOW_GIRL = 1;
    int ERROR_NORES = 2;
    int REGAME = 3;
    int CHANGE_DIFFICULTY = 4;
    int CHANGE_GIRL = 5;
    int COMPLETELY_WON =6;

    int MAX_DIFFICULTI = 5;
    int MIN_DIFFICULTI = 1;

    String USER_WIN = "user_win";
    String COMPUTER_WIN = "computer_win";
    String CHECK_STRING = "check_string";
    String LAST_GIRL = "last_girl";

    //navigation bar
    int ITEM_SCORES = 0;

    //set play style
    int EFFICIENCY_WITHOUT_GAME = 0;
    int EFFICIENCY_TAL_MIN = 1;
    int EFFICIENCY_TAL_MAX = 15;
    int EFFICIENCY_BRIL_MIN = 16;
    int EFFICIENCY_BRIL_MAX = 35;
    int EFFICIENCY_PROF_MIN = 36;
    int EFFICIENCY_PROF_MAX = 55;
    int EFFICIENCY_GEN_MIN = 56;
    int EFFICIENCY_GEN_MAX = 75;
    int EFFICIENCY_CHEM_MIN = 76;
    int EFFICIENCY_CHEM_MAX = 100;

    String ADMOB_BANNER  =  "ca-app-pub-6912796345298703/3273387682";
    String ADMOB_INTERSTITIAL = "ca-app-pub-6912796345298703/3391849324";
    String ADMOB_USER_ID = "ca-app-pub-6912796345298703~4705926847";
}
