package com.dolg.game.scheckers;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

/**
 * Created by Гоша on 04.05.2018.
 */

public class CustomAlertDialog extends Dialog implements View.OnClickListener {

    private ImageButton mainBut,But1,But2,But3,But4,But5,But6,But7,But8,But9;
    private CustomAlertDialog.OnButtonClick onButtonClick;

    public CustomAlertDialog(Context context){
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(com.dolg.game.scheckers.R.layout.alert_dialog);
        initImages();
    }

    public CustomAlertDialog(Context context, int id_image){
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(com.dolg.game.scheckers.R.layout.alert_image);
        mainBut= (ImageButton)findViewById(com.dolg.game.scheckers.R.id.alert_image);
        mainBut.setImageResource(id_image);
        mainBut.setOnClickListener(this);
    }

    public void changeGirlImage(int id_image){
        mainBut.setImageResource(id_image);
    }

    private void initImages(){
        But1 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image1);
        But2 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image2);
        But3 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image3);
        But4 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image4);
        But5 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image5);
        But6 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image6);
        But7 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image7);
        But8 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image8);
        But9 = (ImageButton)findViewById(com.dolg.game.scheckers.R.id.image9);
        But1.setOnClickListener(this);
        But2.setOnClickListener(this);
        But3.setOnClickListener(this);
        But4.setOnClickListener(this);
        But5.setOnClickListener(this);
        But6.setOnClickListener(this);
        But7.setOnClickListener(this);
        But8.setOnClickListener(this);
        But9.setOnClickListener(this);
    }


    public void onClick(View v){
        if(onButtonClick != null) {
                if (v == But1) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL1);
                } else if (v == But2) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL2);
                } else if (v == But3) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL3);
                }
                if (v == But4) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL4);
                } else if (v == But5) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL5);
                }
                if (v == But6) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL6);
                } else if (v == But7) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL7);
                }
                if (v == But8) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL8);
                } else if (v == But9) {
                    onButtonClick.onImageSwitchClick(Const.i_GIRL9);
                } else if (v==mainBut){
                    onButtonClick.onImageButtonClick();
                }
            }

    }

    public void setListener(CustomAlertDialog.OnButtonClick onButtonClick){
        this.onButtonClick = onButtonClick;
    }

    public interface OnButtonClick{
        void onImageSwitchClick(int numb);
        void onImageButtonClick();
    }
}
