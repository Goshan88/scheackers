package com.dolg.game.scheckers;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.google.firebase.crash.FirebaseCrash;
import com.pixplicity.easyprefs.library.Prefs;

/**
 * Created by Гоша on 05.05.2018.
 */

public class ImageHelper {
    private int numbGirl = Const.cntBegin;
    private int cntImageGirl = Const.cntBegin;
    private int rangeImageGirl = Const.cntBegin;
    private int lastGirl = Const.cntBegin;
    private int maxCntGirl=Const.MAX_CNT_GIRL1;
    private Context myContext;
    private Handler mainHandler;
    private boolean UserPlay=false;
    private int cnt_user_win =0, cnt_computer_win =0;

    ImageHelper(Context _context, Handler _handler){
        myContext = _context;
        mainHandler =  _handler;
        cntImageGirl = Const.cntBegin;
        numbGirl = Const.i_GIRL1;
        //load data from memory
        loadScore();
        //set icon last girl
        //setImageIcon();//все рушит
        setDifficulty(cntImageGirl);
    }

    void loadScore(){
        Log.i("loadScore");
        //check data is save
        if(!Prefs.getString(Const.CHECK_STRING,"_").equals(Const.CHECK_STRING)){
            initMemory();
        }else{
            loadDataFromMemory();
        }
    }

    void initMemory(){
        Log.i("initMemory");
        Prefs.putString(Const.CHECK_STRING,Const.CHECK_STRING);
        Prefs.putInt(Const.USER_WIN,0);
        Prefs.putInt(Const.COMPUTER_WIN,0);
        Prefs.putInt(Const.LAST_GIRL,Const.cntBegin);
        for(int i=1; i <= Const.CNT_GIRLS;i++){
            String tmp = getImageRes(i,Const.cntBegin);
            Prefs.putInt(tmp,Const.cntBegin);
        }
    }

    void loadDataFromMemory(){
        Log.i("loadDataFromMemory");
        cnt_user_win = Prefs.getInt(Const.USER_WIN,0);
        cnt_computer_win = Prefs.getInt(Const.COMPUTER_WIN,0);
        lastGirl = Prefs.getInt(Const.LAST_GIRL,Const.cntBegin);
        Log.i(String.valueOf(lastGirl));
        numbGirl = lastGirl;
        cntImageGirl = getCntImageGirl(numbGirl);
        rangeImageGirl = cntImageGirl;
    }

    void incUserWin(){
        Log.i("incUserWin");
        ++cnt_user_win;
        Prefs.putInt(Const.USER_WIN, cnt_user_win);
    }

    void incComputerWin(){
        Log.i("incComputerWin");
        ++cnt_computer_win;
        Prefs.putInt(Const.COMPUTER_WIN, cnt_computer_win);
    }

    int getCntUserWin(){
        return cnt_user_win;
    }

    int getCntComputerWin(){
        return cnt_computer_win;
    }

    void saveCntImageGirl(int id_girl, int cnt_image){
        String tmp = getImageRes(id_girl,Const.cntBegin);
        Prefs.putInt(tmp,cnt_image);
        Log.i("saveCntImageGirl");
        Log.i(String.valueOf(cnt_image));
    }

    int getCntImageGirl(int id_girl){
        int cnt;
        String tmp = getImageRes(id_girl,Const.cntBegin);
        cnt = Prefs.getInt(tmp,Const.cntBegin);
        Log.i("getCntImageGirl");
        Log.i(String.valueOf(cnt));
        return cnt;
    }

    void setImageIcon(){
        int id = getResourceId(getImageRes(numbGirl, cntImageGirl),"drawable",myContext.getPackageName());
        Log.i(String.valueOf(id));
        if(id!=0) {
            sendMsgToUI(Const.CHANGE_ICON,id);
        }else{
            Log.i("Error no res for icon");
            cntImageGirl = Const.cntBegin;
            //sendMsgToUI(Const.ERROR_NORES,id);
        }
    }

    String getImageRes(int id_girl, int cnt_image){
        StringBuffer  sb = new StringBuffer(Const.strGirlName);
        int i = sb.length();
        sb.insert(i,String.valueOf(id_girl));
        i = sb.length();
        sb.insert(i,"_");
        i = sb.length();
        sb.insert(i,String.valueOf(cnt_image));
        return sb.toString();
    }

    void incRangeImage(){
        if(++rangeImageGirl>=maxCntGirl){
            rangeImageGirl = maxCntGirl;
            sendMsgToUI(Const.COMPLETELY_WON,0);
        }
        Log.i("incRangeImage");
        cntImageGirl = rangeImageGirl;
        showImageCurrentGirl();
        setImageIcon();
        setDifficulty(cntImageGirl);
        saveCntImageGirl(numbGirl, rangeImageGirl);
    }

    void decRangeImage(){
        if(--rangeImageGirl<Const.cntBegin){
            rangeImageGirl = Const.cntBegin;
        }
        Log.i("decRangeImage");
        cntImageGirl = rangeImageGirl;
        //showImageCurrentGirl();
        setImageIcon();
        setDifficulty(cntImageGirl);
        saveCntImageGirl(numbGirl, rangeImageGirl);
    }

    void nextGirlImage(){
        if(++cntImageGirl >rangeImageGirl){
            cntImageGirl = Const.cntBegin;
        }
        Log.i(getImageRes(numbGirl, cntImageGirl));
        showImageCurrentGirl();
        setImageIcon();
    }
    /*
    void previousGirlImage(){
        if(--cntImageGirl <Const.cntBegin){
            cntImageGirl = Const.cntBegin;
        }
        Log.i(getImageRes(numbGirl, cntImageGirl));
        //showImageCurrentGirl();//пока так, чтобы не было стыдно за проишранную игру
        setImageIcon();
    }

    */
    void sendMsgToUI(int arg0,int id){
        Message msg = mainHandler.obtainMessage(arg0,id,id);
        mainHandler.sendMessage(msg);
    }

    private int getResourceId(String pVariableName, String pResourcename, String pPackagename){
        try{
            return myContext.getResources().getIdentifier(pVariableName,pResourcename,pPackagename);
        }catch (Exception e){
            FirebaseCrash.report(e);
            Log.e(e.toString());
            return 0;
        }
    }

    void showImageGirl(int id){
        sendMsgToUI(Const.SHOW_GIRL,id);
        Log.i("Method showImageGirl");
    }

    void showImageCurrentGirl(){
        int id = getResourceId(getImageRes(numbGirl, cntImageGirl),"drawable",myContext.getPackageName());
        Log.i(String.valueOf(id));
        if(id!=0) {
            showImageGirl(id);
        }else{
            Log.i("Error no res for image");
            cntImageGirl = Const.cntBegin;
            sendMsgToUI(Const.ERROR_NORES,id);
        }
        Log.i("Method showImageCurrentGirl");
    }

    int getIdCurrentGirl(){
        int id = getResourceId(getImageRes(numbGirl, cntImageGirl),"drawable",myContext.getPackageName());
        Log.i(String.valueOf(id));
        if(id!=0) {
            return id;
        }else{
            Log.i("Error no res for image");
            cntImageGirl = Const.cntBegin;
            sendMsgToUI(Const.ERROR_NORES,id);
        }
        Log.i("Method showImageCurrentGirl");
        return com.dolg.game.scheckers.R.drawable.girl_1_1;
    }

    void clickOnImageGirl(){
        Log.i("Method clickOnImageGirl");
        nextGirlImage();
    }

    void startPlay(){
        if(!UserPlay) {
            Log.i("Method startPlay");
            UserPlay = true;
        }
    }

    void beginGame(){
        Log.i("Method beginGame");
        UserPlay=false;
    }

    void reGame(){
        Log.i("Method beginGame");
        sendMsgToUI(Const.REGAME,0);
        if(UserPlay){
            decRangeImage();
        }
    }

    void winUser(){
        Log.i("winUser");
        nextGirlImage();
        incRangeImage();
        incUserWin();
    }

    void winComputer(){
        Log.i("winComputer");
        decRangeImage();
        incComputerWin();
    }

    void changeGirl(){
        Log.i("winComputer");
        sendMsgToUI(Const.CHANGE_GIRL,0);
        if(UserPlay){
            decRangeImage();
        }
    }

    void setDifficulty(int level){
        if (level<Const.MIN_DIFFICULTI) level = Const.MIN_DIFFICULTI;
        if (level>Const.MAX_DIFFICULTI) level = Const.MAX_DIFFICULTI;
        sendMsgToUI(Const.CHANGE_DIFFICULTY,level);
        Log.i("ImageHelper setDifficulty");
        Log.i(String.valueOf(level));
    }

    void choiceGirl(int idGirl){
        switch(idGirl){
            case Const.i_GIRL1:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL1;
                break;
            case Const.i_GIRL2:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL2;
                break;
            case Const.i_GIRL3:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL3;
                break;
            case Const.i_GIRL4:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL4;
                break;
            case Const.i_GIRL5:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL5;
                break;
            case Const.i_GIRL6:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL6;
                break;
            case Const.i_GIRL7:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL7;
                break;
            case Const.i_GIRL8:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL8;
                break;
            case Const.i_GIRL9:
                numbGirl = idGirl;
                maxCntGirl = Const.MAX_CNT_GIRL9;
                break;

                default:
                numbGirl = Const.cntBegin;
                maxCntGirl = Const.MAX_CNT_GIRL1;
                break;
        }
        rangeImageGirl = getCntImageGirl(numbGirl);
        cntImageGirl = rangeImageGirl;
        Prefs.putInt(Const.LAST_GIRL,idGirl);
        setImageIcon();
        setDifficulty(cntImageGirl);
    }

    int getIdGirl(){
        return numbGirl;
    }

    int getIdImage(){
        return cntImageGirl;
    }

    public void loadIconLastGirl(){
        setImageIcon();
    }

}
