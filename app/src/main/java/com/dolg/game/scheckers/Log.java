package com.dolg.game.scheckers;

/**
 * Created by Гоша on 17.02.2018.
 */
public class Log {
    static final boolean LOG = false;

    public static void i( String string) {
        if (LOG) android.util.Log.i(Const.TAG, string);
    }
    public static void e( String string) {
        if (LOG) android.util.Log.e(Const.TAG, string);
    }
    public static void d(String string) {
        if (LOG) android.util.Log.d(Const.TAG, string);
    }
    public static void v(String string) {
        if (LOG) android.util.Log.v(Const.TAG, string);
    }
    public static void w(String string) {
        if (LOG) android.util.Log.w(Const.TAG, string);
    }
}