package com.dolg.game.scheckers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.pixplicity.easyprefs.library.Prefs;
import com.timqi.sectorprogressview.ColorfulRingProgressView;

import java.util.Random;

public class ScoresActivity extends AppCompatActivity {
    ColorfulRingProgressView spv;
    TextView  PlayStyle,EffecientValue, cntGames;
    int user_win=0, computer_win=0, all_games=0,effiency=0;
    private FirebaseAnalytics mFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.dolg.game.scheckers.R.layout.activity_scores);
        Log.i("ScoreActivity");
        Toolbar toolbar = (Toolbar) findViewById(com.dolg.game.scheckers.R.id.toolbar_scores);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

       spv = (ColorfulRingProgressView)findViewById(com.dolg.game.scheckers.R.id.crpv);
       PlayStyle = (TextView)findViewById(com.dolg.game.scheckers.R.id.play_character);
       EffecientValue = (TextView)findViewById(com.dolg.game.scheckers.R.id.score_value);
       cntGames = (TextView)findViewById(com.dolg.game.scheckers.R.id.value_cnt_games);
       load_scores();

        mFirebaseAnalitics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.VALUE, all_games);
        bundle.putInt(FirebaseAnalytics.Param.SCORE, effiency);
        mFirebaseAnalitics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, bundle);
    }

    void load_scores(){
        Log.i("LoadScores");
        user_win = Prefs.getInt(Const.USER_WIN,0);
        Log.i(String.valueOf(user_win));
        computer_win = Prefs.getInt(Const.COMPUTER_WIN,0);
        Log.i(String.valueOf(computer_win));
        all_games = computer_win + user_win;
        calcEffiency();
        cntGames.setText(String.valueOf(all_games));
    }

    private boolean calcEffiency(){
        Log.i("calcEffiency");
        if(all_games==0){
            effiency = 0;
        }else{
            if(user_win==0){
                Random r = new Random();
                int i1=1;
                try {
                     i1= r.nextInt(9-1) + 1;
                }catch(NullPointerException e){
                    FirebaseCrash.report(e);
                    i1 = r.nextInt(9) + 1;
                }
                effiency = i1;
            }else{
                float fl = ((float)user_win/all_games)*100;
                effiency = Math.round(fl);
            }
        }
        setPlayStyle();
        setEffiency(effiency);
        return true;
    }

    void setPlayStyle() {
        StringBuffer sb=new StringBuffer("");
        if (effiency < Const.EFFICIENCY_TAL_MIN) {
            sb.insert(0, getResources().getString(com.dolg.game.scheckers.R.string.play_the_game));
        } else if (effiency < Const.EFFICIENCY_TAL_MAX) {
            sb.insert(0, getResources().getString(com.dolg.game.scheckers.R.string.talented));
        } else if (effiency < Const.EFFICIENCY_BRIL_MAX) {
            sb.insert(0, getResources().getString(com.dolg.game.scheckers.R.string.brilliant));
        } else if (effiency < Const.EFFICIENCY_PROF_MAX) {
            sb.insert(0, getResources().getString(com.dolg.game.scheckers.R.string.professionally));
        } else if (effiency < Const.EFFICIENCY_GEN_MAX) {
            sb.insert(0, getResources().getString(com.dolg.game.scheckers.R.string.genius));
        } else if (effiency <= Const.EFFICIENCY_CHEM_MAX) {
            sb.insert(0, getResources().getString(com.dolg.game.scheckers.R.string.champion));
        }
        PlayStyle.setText(sb.toString());
    }

    void setEffiency(int value){
        int tmp;
        Log.i("setEffiency");
        Log.i(String.valueOf(value));
        tmp= (value>0) ? value: 0;
        tmp= (value<=100) ? value: 0;
        spv.setPercent(tmp);
        EffecientValue.setText(String.valueOf(tmp));
    }

}
