package core.algorithm;

import com.dolg.game.scheckers.Const;
import com.dolg.game.scheckers.Log;
import com.google.firebase.crash.FirebaseCrash;

import core.board.BoardCell;
import core.rules.GameBoard;
import core.rules.Move;
import core.rules.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class AlphaBetaPruning implements IAlgorithm {
    public static final int LOW_DIFFICULTY = 1;
    public static final int MEDIUM_DIFFICULTY = 3;
    public static final int HIGH_DIFFICULTY = 5;
    private int maxDepth;
    private int mDepth=0;//начало просчета глубины
    public GameBoard gameBoard;
    private LinkedList<MoveWithScore> computerMoves;
    private Move computerMove;

    public class MoveWithScore {
        private Move move;
        double score;

        private MoveWithScore(Move move, double score) {
            this.move = move;
            this.score = score;
        }

        private Move getMove() {
            return move;
        }

        private double getScore() {
            return score;
        }

        public String toString() {
            StringBuffer tmp = new StringBuffer("MoveWithScore: Move-");
            int i = tmp.length();
            tmp.insert(i, move.toString());
            i = tmp.length();
            tmp.insert(i, ", Score-");
            i = tmp.length();
            tmp.insert(i, String.valueOf(score));
            return tmp.toString();
        }
    }

    public void GetScore() {
        getHeuristicEvaluation();
    }

    public void setMaxDepth(int value){
        int tmp=0;
        if (value< Const.MIN_DIFFICULTI) tmp = Const.MIN_DIFFICULTI;
        if (value>Const.MAX_DIFFICULTI) tmp = Const.MAX_DIFFICULTI;
        Log.i("setMaxDepth");
        Log.i(String.valueOf(value));
        maxDepth = tmp;
    }
    public int getMaxDepth(){
        return maxDepth;
    }

    public AlphaBetaPruning(GameBoard gameBoard, int difficutly) {
        this.maxDepth = difficutly;
        this.gameBoard = gameBoard;
        this.computerMove = null;
        computerMoves = new LinkedList<>();
        //Log.i("constructor alphaBetaPruning _ 1");
        GetScore();
    }

    @Override
    public AlgorithmType getAlgorithmType() {
        return AlgorithmType.COMPUTER;
    }

    @Override
    public Move getOpponentMove() {
        //Log.i("getOpponentMove");
        if (computerMove == null) {
            //Log.i("computerMove == null");
            Random random = new Random(System.currentTimeMillis());
            if (computerMoves != null) {
                for (MoveWithScore m : computerMoves) {
                   // Log.i(m.toString());
                }
            }
            /*E/AndroidRuntime: FATAL EXCEPTION: Thread-107
                  Process: com.intel.samples.scheckers, PID: 19306
                  java.lang.IllegalArgumentException: n must be positive
                      at java.util.Random.nextInt(Random.java:391)*/
            try {
                computerMove = computerMoves.get(random.nextInt(computerMoves.size())).getMove();
            }catch (NullPointerException e){
                computerMove = computerMoves.get(0).getMove();//если появилась ошибка то берем просто первый ход
                FirebaseCrash.report(e);
            }
            // StringBuffer sb = new StringBuffer("getOpponentMove return: ");
           // int i = sb.length();
           // sb.insert(i, computerMove.toString());
           // Log.i(sb.toString());
        }
        return computerMove;
    }



    @Override
    public void getAlgorithm(Player player) {
        computerMove = null;
        computerMoves.clear();
        alphaBetaPruning(mDepth, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, player, null);
        //Log.i("getAlgorithm");
    }

    private double alphaBetaPruning(int depth, double alpha, double beta, Player player, BoardCell requiredMoveCell) {
        boolean next_eatmove=false;
        if (gameBoard.hasWon(player))
            return player.getPieceColor();

        List<Move> availiableMoves;
        if (requiredMoveCell == null) {
            availiableMoves = gameBoard.getAllAvailiableMoves(player);
        } else {
            availiableMoves = gameBoard.getAvailiableMoves(requiredMoveCell);
        }


        if ((availiableMoves.isEmpty())||(depth == maxDepth)){
                    return getHeuristicEvaluation();
        }

        next_eatmove = gameBoard.getPlayerEat(player);

        // Max player
        if (player == Player.WHITE) {
            //Log.i("Max player Player.WHITE");
            double score = Double.NEGATIVE_INFINITY;
            for (Move m : availiableMoves) {
                BoardCell fromCell = new BoardCell(m.getFromCell());
                BoardCell toCell = new BoardCell(m.getToCell());
                BoardCell eatCell = new BoardCell(m.getEatCell());

                double step_score= Double.NEGATIVE_INFINITY;
                if(next_eatmove) {
                    int eatDepth = (depth == 0) ? (depth + 1) : depth;
                    if (m.getToCell().isKingPiece())
                        eatDepth++;
                    step_score = alphaBetaPruning(eatDepth, alpha, beta, player, m.getToCell());
                }
                gameBoard.doMove(m);

                if (gameBoard.getPlayerEat(Player.WHITE)) {

                } else {
                    step_score = alphaBetaPruning(depth + 1, alpha, beta, player.getOpposite(), null);
                }
                gameBoard.setCell(fromCell.getRow(), fromCell.getCol(), fromCell);
                gameBoard.setCell(toCell.getRow(), toCell.getCol(), toCell);
                gameBoard.setCell(eatCell.getRow(), eatCell.getCol(), eatCell);

                if (score < step_score) {
                    score = step_score;
                }
                if (alpha < score) {
                    alpha = score;
                }
                if (beta <= alpha) {
                    break;
                }
            }
            return score;
        }

        // Min player
        if (player == Player.BLACK) {
            //Log.i("Min player Player.BLACK");
            double score = Double.POSITIVE_INFINITY;
            for (Move m : availiableMoves) {
                BoardCell fromCell = new BoardCell(m.getFromCell());
                BoardCell toCell = new BoardCell(m.getToCell());
                BoardCell eatCell = new BoardCell(m.getEatCell());

                double step_score = Double.POSITIVE_INFINITY;
                if(next_eatmove){
                    int eatDepth = (depth == 0) ? (depth + 1) : depth;
                    if (m.getToCell().isKingPiece())
                        eatDepth++;
                    step_score = alphaBetaPruning(eatDepth, alpha, beta, player, m.getToCell());

                }
                gameBoard.doMove(m);


                if (gameBoard.getPlayerEat(Player.WHITE)) {

                } else {
                   // Log.i("Black player move:");
                   // Log.i(m.toString());
                    step_score = alphaBetaPruning(depth + 1, alpha, beta, player.getOpposite(), null);
                }
                gameBoard.setCell(fromCell.getRow(), fromCell.getCol(), fromCell);
                gameBoard.setCell(toCell.getRow(), toCell.getCol(), toCell);
                gameBoard.setCell(eatCell.getRow(), eatCell.getCol(), eatCell);

                if (score >= step_score) {
                    score = step_score;
                    if (depth == 0) {
                        for (MoveWithScore mws : new LinkedList<>(computerMoves)) {
                            if (mws.getScore() > score)
                                computerMoves.remove(mws);
                        }
                        computerMoves.add(new MoveWithScore(new Move(m), score));
                       // Log.i(computerMoves.getLast().toString());
                    }
                }
                if (beta > score) {
                    beta = score;
                }
                if (beta <= alpha && depth > 0) {
                    break;
                }
            }
            return score;
        }
        return Double.POSITIVE_INFINITY;
    }


    public double getHeuristicEvaluation() {
        double count = 0;
       // Log.i(gameBoard.toString());
        double kingWeight = Math.abs(BoardCell.WHITE_PIECE) /2.0;
        double pieceWeight = Math.abs(BoardCell.WHITE_PIECE) /6.0;
        double coneWeight = Math.abs(BoardCell.WHITE_PIECE) /12.0;
        for (int row = 0; row < GameBoard.CELL_COUNT; ++row) {
            for (int col = 0; col < GameBoard.CELL_COUNT; ++col) {
                switch (gameBoard.getCell(row, col).getCondition()) {
                    case BoardCell.BLACK_PIECE:
                        if (gameBoard.getCell(row, col).isKingPiece())
                            count -= kingWeight;
                        else
                            count -= pieceWeight;
                        break;
                    case BoardCell.WHITE_PIECE:
                        if (gameBoard.getCell(row, col).isKingPiece())
                            count += kingWeight;
                        else
                            count += pieceWeight;
                    default:
                        break;
                }
                //занимаем углы
                    if ((col==0)||(col==GameBoard.CELL_COUNT)){
                        if(gameBoard.getCell(row,col).getCondition()==BoardCell.BLACK_PIECE){
                            count -= coneWeight;
                        }else{
                            count += coneWeight;
                        }
                }
            }
        }



        StringBuffer tmp = new StringBuffer("getHeuristicEvaluation: Score-");
        int i=tmp.length();
        tmp.insert(i,String.valueOf(count));
        //Log.i(tmp.toString());
        return count;
    }
}
