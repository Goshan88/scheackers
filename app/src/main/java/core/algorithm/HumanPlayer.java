package core.algorithm;

import core.rules.Move;
import core.rules.Player;

public class HumanPlayer implements IAlgorithm {
    public HumanPlayer() {}

    @Override
    public void getAlgorithm(Player player) {}

    @Override
    public AlgorithmType getAlgorithmType() {
        return AlgorithmType.HUMAN;
    }

    @Override
    public Move getOpponentMove() {
        return null;
    }

    @Override
    public void setMaxDepth(int value) {
    }
}
