package core.algorithm;

import core.rules.Move;
import core.rules.Player;

public interface IAlgorithm {
    void getAlgorithm(Player player);
    AlgorithmType getAlgorithmType();
    Move getOpponentMove();
    void setMaxDepth(int value);
}
